import React, { Suspense, useState } from "react";
import ErrorBoundary from "./ErrorBoundary";

const List = React.lazy(() => import("ProductList/List"));

const ProductList = () => {
  const [err, setErr] = useState(false);
  return (
    <Suspense fallback="Loading...">
      <ErrorBoundary fallback={"Fallback Error Boundary"}>
        <button onClick={() => setErr(!err)}>Set Err</button>
        {err && <List />}
      </ErrorBoundary>
    </Suspense>
  );
};

export default ProductList;
