import React, { Suspense } from "react";
import ErrorBoundary from "./ErrorBoundary";

const Button = React.lazy(() => import("Header/Button"));
const List = React.lazy(() => import("ProductList/List"));
const BtnVite = React.lazy(() => import("remoteApp/Button"));

const App = () => (
  <div>
    <h1>Webpack Shell</h1>
    Webpack Header: <Button />
    <Suspense fallback="Failed to load...">
      <List />
    </Suspense>
    <ErrorBoundary fallback="Vite Error">
      Vite Remote: <BtnVite />
    </ErrorBoundary>
  </div>
);

export default App;
