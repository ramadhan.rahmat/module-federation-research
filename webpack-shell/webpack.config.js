const HtmlWebpackPlugin = require("html-webpack-plugin");
const { ModuleFederationPlugin } = require("webpack").container;
const path = require("path");

const htmlPlugin = new HtmlWebpackPlugin({
  template: "./public/index.html",
  filename: "./index.html",
});

/** @type { import('webpack').Configuration } */
module.exports = {
  mode: "development",
  devServer: {
    static: path.join(__dirname, "dist"),
    port: 5000,
    historyApiFallback: {
      index: "/public/index.html",
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },
  plugins: [
    htmlPlugin,
    new ModuleFederationPlugin({
      name: "ShellApplication",
      filename: "remoteEntry.js",
      remotes: {
        remoteApp: "app@http://localhost:4001/assets/remoteEntry.js",
        Header: "Header@http://localhost:5001/remoteEntry.js",
        ProductList: "ProductList@http://localhost:5002/remoteEntry.js",
      },
    }),
  ],
};
