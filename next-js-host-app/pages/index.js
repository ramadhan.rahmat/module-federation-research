import dynamic from "next/dynamic";
import Button from "../components/Button";
import React, { lazy, useEffect, useState } from "react";

export default function Home() {
  const [Component, setComponent] = useState(null);
  const [Vite, setVite] = useState(null);
  const [Webpack, setWebpack] = useState(null);
  console.log({ Webpack });
  useEffect(() => {
    if (typeof window !== "undefined") {
      setComponent(lazy(() => import("remote/Button")));
      setWebpack(lazy(() => import("Webpack/List")));
      setVite(lazy(() => import("Vite/Button")));
    }
  }, []);
  return (
    <div style={{ padding: "2%" }}>
      <h1>Next JS and React</h1>
      <h2>Host - Button</h2>
      <Button />
      <h2>Client - Button</h2>
      Vite Remote: {Vite && <Vite />}
      <br />
      <br />
      Webpack Remote: {Component && <Component />}
      <br />
      <br />
      Webpack Product List:{Webpack && <Webpack />}
    </div>
  );
}
