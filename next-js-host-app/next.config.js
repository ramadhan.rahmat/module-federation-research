const { NextFederationPlugin } = require("@module-federation/nextjs-mf");

module.exports = {
  webpack(config, options) {
    if (!options.isServer) {
      config.plugins.push(
        new NextFederationPlugin({
          name: "host",
          library: {
            type: "commonjs",
          },
          extraOptions: {
            debug: true,
          },
          remoteType: "commonjs",
          remotes: {
            Webpack: "ProductList@http://localhost:5002/remoteEntry.js",
            Vite: "app@http://localhost:4001/assets/remoteEntry.js",
            remote: "remote@http://localhost:3001/remote.js",
          },
          filename: "static/chunks/remoteEntry.js",
        })
      );
    }

    return config;
  },
};
