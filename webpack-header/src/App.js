import React from "react";
import Button from "./Button";

const App = () => (
  <div>
    Header Module <Button />
  </div>
);

export default App;
