## Module Federation Research

### Port

- Next Js Host App: 3000
- Webpack Next Js Remote App: 3001
  
- Vite Host: 4000
- Vite Remote 4001
  
- Webpack Shell: 5000
- Webpack Header: 5001
- Webpack ProductList: 5002

## How to run

### Next Js Host

- Run next js host using `sudo yarn dev`
- Run webpack ProductList using `sudo yarn start`
- Run vite remote using `sudo yarn build && sudo yarn serve`
- Run webpack next remote using `sudo yarn dev`

### Vite Host

- Run vite host using `sudo yarn dev`
- Run vite remote using `sudo yarn build && sudo yarn serve`
- Run webpack ProductList using `sudo yarn start`

## Webpack Shell Host

- Run vite remote using `sudo yarn build && sudo yarn serve`
- Run webpack ProductList using `sudo yarn start`
- Run webpack Header using `sudo yarn start`
- Run webpack shell host using `sudo yarn start`
