import React from "react";

const List = () => (
  <>
    {new Array(10).fill("").map((_, i) => (
      <React.Fragment key={i}>
        <div>Product {i}</div>
        <br />
      </React.Fragment>
    ))}
  </>
);

export default List;
